FROM registry.gitlab.com/ercom/docker:18.03.1-ce-git

COPY docker-entrypoint.sh /tmp/docker-entrypoint.sh
RUN (head -n16 /tmp/docker-entrypoint.sh \
    && sed '/exec .*$/d' /usr/local/bin/docker-entrypoint.sh \
    && tail -n+18 /tmp/docker-entrypoint.sh) \
    | awk '{ b = (NR > 1 ? b""ORS""$0 : $0); } END{ print b > "/usr/local/bin/docker-entrypoint.sh"; }' \
    && rm /tmp/docker-entrypoint.sh
