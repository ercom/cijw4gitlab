# CI Job wrapper

Run a job with extra capabilities (compared to gitlab-ci runner) like attach a volume from an other image.

## Table of content

  * [Standard Gitlab job](#standard-gitlab-job)
    + [Initial state: before run the build](#initial-state-before-run-the-build)
    + [Step 1: "build" data-only container creation](#step-1-build-data-only-container-creation)
    + [Step 2: Clone or fetch the repository](#step-2-clone-or-fetch-the-repository)
    + [Step 3: Run the job container](#step-3-run-the-job-container)
    + [Step 4: Execute the job scripts](#step-4-execute-the-job-scripts)
    + [Step 5: Container job end](#step-5-container-job-end)
    + [After the job](#after-the-job)
  * [The CI job wrapper](#the-ci-job-wrapper)
    + [Job setting file](#job-setting-file)
    + [The property list](#the-property-list)
  * [Advanced job with the CI job wrapper](#advanced-job-with-the-ci-job-wrapper)
    + [Initial state: before run the build](#initial-state-before-run-the-build-1)
    + [Step 1: "build" data-only container creation](#step-1-build-data-only-container-creation-1)
    + [Step 2: Clone or fetch the repository](#step-2-clone-or-fetch-the-repository-1)
    + [Step 3: Run the job container (CI job wrapper)](#step-3-run-the-job-container-ci-job-wrapper)
    + [Step 4: Run the CI job wrapper docker entry point](#step-4-run-the-ci-job-wrapper-docker-entry-point)
    + [Step 5: Setup additional CI volumes data-only containers](#step-5-setup-additional-ci-volumes-data-only-containers)
    + [Step 6: Run the final job container](#step-6-run-the-final-job-container)
    + [Step 7: Execute the job scripts](#step-7-execute-the-job-scripts)
    + [Step 8: Container job end](#step-8-container-job-end)
    + [Step 9: CI job wrapper end](#step-9-ci-job-wrapper-end)
    + [After the job](#after-the-job-1)
  * [Example of job using the wrapper](#example-of-job-using-the-wrapper)

## Standard Gitlab job

For this example, we can use the following job description:

```yaml
test-ci-job-wrapper:
  image: ubuntu:16.04
  stage: build
  script:
    - echo "I'm a standard job"
```

### Initial state: before run the build

If you start Gitlab runner docker for the first time from a fresh install, you will have only one started gitlab-runner container

![Gitlab runner start](doc/UseCaseBaseStepStart.png)

State:

Container - total: 1, running: 1

### Step 1: "build" data-only container creation

Before run the build, the gitlab-runner create a cache container to store the workspace

* It's created only if don't exists (this is a persistent container)
* It contains one volume: /builds/$CI_PROJECT_NAME
* This container is never started (data-only container)
* There are one "build" cache per concurrent per project
* The container name is under the format ```runner-<ID>-project-<NO>-concurrent-<NO>-cache-<HASH>```

![Use case step 1](doc/UseCaseBaseStep1.png)

Container - total: 2, running: 1

### Step 2: Clone or fetch the repository

Clone or fetch and checkout the suitable repository into /builds/$CI_PROJECT_NAME workspace

![Use case step 2](doc/UseCaseBaseStep2.png)

Container - total: 2, running: 1

### Step 3: Run the job container

The gitlab-runner create and start the job container (ubuntu:16.04)

* created before each job execution and destroyed at the end
* inherit all volumes (/builds/$CI_PROJECT_NAME, etc.) from ```runner-<ID>-project-<NO>-concurrent-<NO>-cache-<HASH>``` data only containers

![Use case step 3](doc/UseCaseBaseStep3.png)

Container - total: 3, running: 2

### Step 4: Execute the job scripts

The final job container will execute the scripts given by gitlab-runner.

![Use case step 4](doc/UseCaseBaseStep4.png)

Container - total: 6, running: 3

### Step 5: Container job end

![Use case step 5](doc/UseCaseBaseStep5.png)

Container - total: 2, running: 1

### After the job

After the job, the job docker is destroyed

![Use case step End](doc/UseCaseBaseStepEnd.png)

Container - total: 2, running: 1

## The CI job wrapper

Now, if you want to add specific options and volume, depending each job, you will have to edit a specific runner(s) with the suitable configuration for each case statically.

The other way designed here is to use a special job wrapper with a setting file .ci-job-wrapper.yml stored into the project (like the .gitlab-ci.yml)

IMPORTANT: The CI job wrapper container need an access to the same docker container daemon, the one used by the gitlab-runner. Usually, you may have to add an access to the host /var/run/docker.sock (add the suitable volume into config.toml)

### Job setting file

For each job using the CI job wrapper, ￼there must be an equivalent in the .ci-job-wrapper.yml file:

```yaml
my-job-name:
    property1: value1
    property2: value2...
```

### The property list

* ```image``` (String): The final/target main docker image
* ```volumes_from_images``` (String or array of String): list of docker image to attach to the target job container
* ```docker_run_args``` (Object): Add docker run additional arguments to the job container where each object key is translated to a docker long argument. Each argument can be a string, boolean, number and an array of elements (useful to specify several time a same argument, e.g.: ```publish: [443, 2222:22]``` is translated to ```--publish 443 --publish 2222:22```)
* ```services```: (Array of String/Object) Add a service(s) as a Gitlab service ([more details here][https://docs.gitlab.com/ce/ci/docker/using_docker_images.html#what-is-a-service]) with additionals properties:
    * ```wait_before_run_job````: Time to wait before run the job (default: 0)
    * ```docker_run_args``` (Object): Service docker run options like the job one

Network mode, option ```docker_run_args```.```net```: by default (unset), the target container share the network stack with the CI job wrapper one started by the gitlab-runner service. You can specify values like 'bridge' to allocate a new net stack, 'host' to use the host one, or 'none' to disable network

Privileged mode, option ```docker_run_args```.```privileged```: at ```true``` start a privileged container

## Advanced job with the CI job wrapper

For this example, we can use the following job description:

```yaml
test-ci-job-wrapper:
  image: cijw4gitlab:1.0
  stage: build
  script:
    - echo "Advanced job"
```

This job must have the equivalent into the file ```.ci-job-wrapper.yml```:
```yaml
test-ci-job-wrapper:
  image: ubuntu:16.04
  volumes_from_images:
    - data-only-image1:vol1
    - data-only-image2:vol2
```

### Initial state: before run the build

If you start Gitlab runner docker for the first time from a fresh install, you will have only one started gitlab-runner container

![Gitlab runner start](doc/UseCaseAdvStepStart.png)

State:

Container - total: 1, running: 1

### Step 1: "build" data-only container creation

Before run the build, the gitlab-runner create a cache container to store the workspace

* It's created only if don't exists (this is a persistent container)
* It contains one volume: /builds/$CI_PROJECT_NAME
* This container is never started (data-only container)
* There are one "build" cache per concurrent per project
* The container name is under the format ```runner-<ID>-project-<NO>-concurrent-<NO>-cache-<HASH>```

![Use case step 1](doc/UseCaseAdvStep1.png)

Container - total: 2, running: 1

### Step 2: Clone or fetch the repository

Clone or fetch and checkout the suitable repository into /builds/$CI_PROJECT_NAME workspace

![Use case step 2](doc/UseCaseAdvStep2.png)

Container - total: 2, running: 1

### Step 3: Run the job container (CI job wrapper)

The gitlab-runner create and start the job container (cijw4gitlab:1.0)

* created before each job execution and destroyed at the end
* inherit all volumes (/builds/$CI_PROJECT_NAME, etc.) from ```runner-<ID>-project-<NO>-concurrent-<NO>-cache-<HASH>``` data only containers

This wrapper image contains an Docker entry point

![Use case step 3](doc/UseCaseAdvStep3.png)

Container - total: 3, running: 2

### Step 4: Run the CI job wrapper docker entry point

This wrapper image contains an Docker entry point, automatically executed on docker start

![Use case step 4](doc/UseCaseAdvStep4.png)

Container - total: 3, running: 2


### Step 5: Setup additional CI volumes data-only containers

The gitlab-runner create and start the suitable job container (ubuntu:16.04 in this case)

* created before each job execution and destroyed at the end
* inherit all volumes (/builds/$CI_PROJECT_NAME, etc.) from ```runner-<ID>-project-<NO>-concurrent-<NO>-cache-<HASH>``` data only containers

![Use case step 5](doc/UseCaseAdvStep5.png)

Container - total: 5, running: 2

### Step 6: Run the final job container

The ci-job-wrapper create and start the suitable job container (ubuntu:16.04 in this case)

* created before each job execution and destroyed at the end
* inherit all volumes from the build project runner and the CI additional volumes

![Use case step 6](doc/UseCaseAdvStep6.png)

Container - total: 6, running: 3

### Step 7: Execute the job scripts

The final job container will execute the scripts given by gitlab-runner relayed by the CI job wrapper.

![Use case step 7](doc/UseCaseAdvStep7.png)

Container - total: 6, running: 3

### Step 8: Container job end

![Use case step 8](doc/UseCaseAdvStep8.png)

Container - total: 5, running: 2

### Step 9: CI job wrapper end

![Use case step 9](doc/UseCaseAdvStep9.png)

Container - total: 4, running: 1

### After the job

After the job, the job docker is destroyed

![Use case step 1](doc/UseCaseAdvStepEnd.png)

Container - total: 4, running: 1

## Example of job using the wrapper

Important: This docker image need an access to the host /var/run/docker.sock (add the suitable volume indo config.toml) to run an other container on the same container that gitlab-runner.

Example of a job in the ```.gitlab-ci.yml```:

```yaml
test-ci-job-wrapper:
  image: ${CI_REGISTRY}/build/cijw4gitlab:1.0
  stage: build
  script:
    - foo/bar.sh
```

This job must have the equivalent into the file ```.ci-job-wrapper.yml```:
```yaml
test-ci-job-wrapper:
  # [mandatory] Target main docker image
  image: ${CI_REGISTRY}/build/linux:1.0
  # [optional] image list
  volumes_from_images:
    - ${CI_REGISTRY}/poc/imgrs1:1.1
    - ${CI_REGISTRY}/poc/imgrs2:1.2
  # [optional] [expert] docker run additional argument
  docker_run_args:
    memory: 512m
    read-only: true
    publish:
      - 443
      - 2222:22
      - 127.0.0.1:80:8080

```

In this case, a docker ```build/linux:1.0``` will be launched with the volumes from ```poc/imgrs1:1.1``` and ```poc/imgrs2:1.2```

It will also add ```--memory "512m" --read-only --publish 443  --publish 2222:22 --publish 127.0.0.1:80:8080``` options the docker run command

An example of job (name: job-with-volumes) using this image can be found [here](https://gitlab.com/ercom/cilib4gitlab)
