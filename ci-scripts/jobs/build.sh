
citbx_use "dockerimg"

job_main() {
    docker build -t ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG} .
    if [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN ${CI_REGISTRY};
        docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG};
    fi
}

job_after() {
    local retcode=$1
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG\" successfully generated"
    fi
}
