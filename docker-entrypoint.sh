#!/bin/bash -e
# cijw4gitlab: CI job wrapper for Gitlab
# Copyright (C) 2017 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

print_critical() {
    printf "\033[91m[CI-Job-Wrapper::CRIT] %s\033[0m\n" "$@"
    exit 1
}

print_error() {
    printf "\033[91m[CI-Job-Wrapper::ERRO] %s\033[0m\n" "$@"
}

print_warning() {
    printf "\033[93m[CI-Job-Wrapper::WARN] %s\033[0m\n" "$@"
}

if [ "$CIJW_DEBUG" == "true" ]; then
    print_note() {
        printf "[CI-Job-Wrapper::NOTE] %s\n" "$@"
    }
else
    print_note() {
        return 0
    }
fi

print_info() {
    printf "\033[92m[CI-Job-Wrapper::INFO] %s\033[0m\n" "$@"
}

yaml2json() {
    cat "$@" | python -c 'import sys, yaml, json; json.dump(yaml.load(sys.stdin), sys.stdout)'
}

# Detect if the environment is in interactive mode or not
if [ -t 1 ]; then
    CIJW_JOB_DOCKER_RUN_ARGS+=(-ti)
else
    CIJW_JOB_DOCKER_RUN_ARGS+=(-i --attach=STDOUT --attach=STDERR)
fi

# Sanity checks
cd $CI_PROJECT_DIR

test -n "$CI" \
    || print_critical "This image must be started in a CI environment"

test -n "$CI_JOB_NAME" \
    || print_critical "Undefined variable CI_JOB_NAME !"

test -f .ci-job-wrapper.yml \
    || print_critical "No .ci-job-wrapper.yml file found"

if [ -z "$CIJW_CONFIG_JSON" ]; then
    if [ -n "$CIJW_CONFIG_YAML" ]; then
        CIJW_CONFIG_JSON=$(yaml2json <<< "$CIJW_CONFIG_YAML")
    else
        if [ ! -f ".ci-job-wrapper.yml" ]; then
            print_critical ".ci-job-wrapper.yml file not found"
        fi
        print_note "Read the file .ci-job-wrapper.yml"
        CIJW_CONFIG_JSON=$(yaml2json .ci-job-wrapper.yml)
    fi
fi

ci_jw_query() {
    jq "$@" <<< "$CIJW_CONFIG_JSON"
}

# Check job properties
for k in $(ci_jw_query -r '."'"$CI_JOB_NAME"'" | keys | .[]'); do
    print_note "Chech job property $CI_JOB_NAME.$k"
    case "$k" in
        image|volumes_from_images|docker_run_args|services)
            ;;
        *)
            print_critical "Unreconised (or no longer available) '$k' property for the job '$CI_JOB_NAME'"
            ;;
    esac
done

# Docker ID
CIJW_ID=$(head -c 8 /dev/urandom | od -t x8 -An | grep -oE '\w+')
CIJW_DOCKER_PREFIX="cijw-$CIJW_ID"

# Extract Add docker ID and add volume from this docker
CIJW_DOCKER_ID="$(cat /proc/self/cgroup | gawk 'match($0, /([a-f0-9]+)$/, ret) {print ret[1]; exit}')"
test -n "$CIJW_DOCKER_ID" \
    || print_critical "Unable to extract docker ID"
print_note "Add volume: $CIJW_DOCKER_ID"
CIJW_JOB_DOCKER_RUN_ARGS+=(--volumes-from "$CIJW_DOCKER_ID")

# Transmit CI* env variables to the new environment
for e in $(cat /proc/self/environ | tr -d '\n' | tr '\000' '\n' | cut -d '=' -f 1); do
    case "$e" in
        _|HOSTNAME|OLDPWD|PATH|PWD|SHLVL|HOME|USER)
            ;;
        *)
            CIJW_DOCKER_RUN_ARGS+=(-e "$e")
            ;;
    esac
done

# Extract job image properties
CIJW_JOB_DOCKER_COMMANDS=()
for p in '."'"$CI_JOB_NAME"'"' ''; do
    case "$(ci_jw_query -r "$p.image | type")" in
        object)
            if [ "$(ci_jw_query -r "$p.image.name | type")" == "string" ]; then
                CIJW_JOB_DOCKER_IMAGE="$(eval echo "$(ci_jw_query "$p.image.name")")"
                if [ "$(ci_jw_query -r "$p.image.entrypoint | type")" == "array" ]; then
                    for i in $(seq 0 $(ci_jw_query -r "$p.image.entrypoint | length - 1")); do
                        CIJW_JOB_DOCKER_COMMANDS+=("$(eval echo "$(ci_jw_query "$p.image.entrypoint[$i]")")")
                    done
                fi
                break
            fi
            ;;
        string)
            CIJW_JOB_DOCKER_IMAGE="$(eval echo "$(ci_jw_query "$p.image")")"
            break
            ;;
        *)
            ;;
    esac
done

# Test job image
test -n "$CIJW_JOB_DOCKER_IMAGE" \
    || print_critical "Undefined image property for the job '$CI_JOB_NAME' into .ci-job-wrapper.yml"
print_note "Image name: $CIJW_JOB_DOCKER_IMAGE"

case "$(ci_jw_query -r '."'"$CI_JOB_NAME"'".docker_run_args.network | type')" in
    string)
        CIJW_JOB_DOCKER_NET="$(eval echo "$(ci_jw_query '."'"$CI_JOB_NAME"'".docker_run_args.network')")"
        ;;
    ''|null)
        ;;
    *)
        print_critical '."'"$CI_JOB_NAME"'".docker_run_args.network'" value must be a string"
        ;;
esac

# Add notwork mode
case "$CIJW_JOB_DOCKER_NET" in
    '')
        CIJW_JOB_DOCKER_NET="container:$CIJW_DOCKER_ID"
        CIJW_JOB_DOCKER_RUN_ARGS+=(--network "$CIJW_JOB_DOCKER_NET")
        ;;
    container:*|host)
        ;;
    *)
        CIJW_JOB_DOCKER_RUN_ARGS+=(--hostname "$HOSTNAME")
        CIJW_NETWORK_EDITABLE="true"
        ;;
esac

process_docker_run_args() {
    local tg_list=$1
    local perfix=$2
    case "$(ci_jw_query -r "$perfix"'."docker_run_args" | type')" in
        object)
            ;;
        ''|null)
            return
            ;;
        *)
            print_critical "$perfix.docker_run_args must be an object"
            ;;
    esac
    local run_args_obj=$(ci_jw_query -r "$perfix"'."docker_run_args"')
    local args=()
    for p in $(jq -r 'keys | .[]' <<< $run_args_obj); do
        case "$(jq -r '."'"$p"'" | type' <<< $run_args_obj)" in
            boolean)
                if [ "$(jq -r '."'"$p"'"' <<< $run_args_obj)" == "true" ]; then
                    args+=("--$p")
                fi
                ;;
            string|number)
                args+=("--$p" "$(eval echo "$(jq '."'"$p"'"' <<< $run_args_obj)")")
                ;;
            array)
                while read -r line; do
                    args+=("--$p" "$(eval echo "$line")")
                done <<< "$(jq -r '."'"$p"'"[]' <<< $run_args_obj)"
                ;;
            *)
                print_critical "$perfix.docker_run_args only support boolean, string, number or array elements"
                ;;
        esac
    done
    eval "$tg_list"'+=("${args[@]}")'
}

# Process docker_run_args job property and append to CIJW_JOB_DOCKER_RUN_ARGS array
process_docker_run_args CIJW_JOB_DOCKER_RUN_ARGS '."'"$CI_JOB_NAME"'"'

# copy DOCKER_AUTH_CONFIG into ~/.docker/config.json
if [ -n "$DOCKER_AUTH_CONFIG" ]; then
    mkdir -p ~/.docker/
    echo "$DOCKER_AUTH_CONFIG" > ~/.docker/config.json
fi

# Get the volume image list
docker_volumes=()
if [ "$(ci_jw_query -r '."'"$CI_JOB_NAME"'"."volumes_from_images" | type')" == "array" ]; then
    while read -r line; do
        docker_volumes+=("$(eval echo $line)")
    done <<< "$(ci_jw_query '."'"$CI_JOB_NAME"'"."volumes_from_images"[]')"
fi

# Check and create volumes if needed
for image in "${docker_volumes[@]}"; do
    cnt_ref="ci_volume_$(sha256sum  <<< "$image" | cut -d\  -f 1)"
    print_note "Check for volume $cnt_ref..."
    remaining_try=${CIJW_VOL_CREATE_TRY_COUNT:-10}
    until [ "$(docker inspect $cnt_ref 2>/dev/null | jq -r '.[].State.Status')" == "created" ]; do
        print_info "Creating volume $cnt_ref from image $image..."
        # An error can occur when another wrapper try to create the same container
        # at the same time, and in this case, we can just ignore this error...
        if docker create --name $cnt_ref $image; then
            break
        elif ((--remaining_try)); then
            print_warning "Will retry to create the volume in a little while..."
            sleep ${CIJW_VOL_CREATE_TRY_INTVL:-10}
        else
            print_critical "Aborting due to too much errors"
        fi
    done
    print_note "Add volume: $cnt_ref"
    CIJW_JOB_DOCKER_RUN_ARGS+=(--volumes-from $cnt_ref)
done

if [[ -v DOCKER_RUN_EXTRA_ARGS ]]; then
    # Add inherited extra docker options
    eval "CIJW_CI_DOCKER_EXTRA_ARGS=$DOCKER_RUN_EXTRA_ARGS"
    for op in "${CIJW_CI_DOCKER_EXTRA_ARGS[@]}"; do
        if [ "$op" == "--network" ]; then
            network_op="true"
        fi
        CIJW_JOB_DOCKER_RUN_ARGS+=("$op")
    done
fi

# Clean previous one not stopped normally
if docker ps -a | grep -q "${HOSTNAME}-ci-job-wrapper$"; then
    print_note "Remove old ${HOSTNAME}-ci-job-wrapper docker"
    docker rm -f "${HOSTNAME}-ci-job-wrapper"
fi

# hook executed on exit
executor_docker_exit_hook() {
    test -n "$CIJW_DOCKER_PREFIX" || print_critical "Assert: empty CIJW_DOCKER_PREFIX"
    for d in $(docker ps -a --filter "label=$CIJW_DOCKER_PREFIX" -q); do
        print_note "Cleanup docker $d..."
        docker rm -f $d > /dev/null 2>&1 || true
    done
}
trap executor_docker_exit_hook EXIT SIGINT SIGTERM

wait_before_run_job=0

# Start a service
start_docker_service() {
    local image=$1
    local name=$2
    local network=$3
    local ip
    CIJW_SRV_DOCKER_RUN_ARGS+=(--name "$CIJW_DOCKER_PREFIX-$name" --label "$CIJW_DOCKER_PREFIX")
    shift 3
    if [ -n "$1" ]; then
        CIJW_SRV_DOCKER_RUN_ARGS+=(--entrypoint "$1")
    fi
    shift || true
    print_info "Starting service $name..."
    docker run -d "${args[@]}" "${CIJW_DOCKER_RUN_ARGS[@]}" "${CIJW_SRV_DOCKER_RUN_ARGS[@]}" "$image" "$@"
    case "$network" in
        host|container:*)
            return 0
            ;;
        *)
            ;;
    esac
    case "$CIJW_JOB_DOCKER_NET" in
        host|container:*)
            print_warning "Cannot add '--add-host' argument on the job container with network mode '$CIJW_JOB_DOCKER_NET'"
            return 0
            ;;
        *)
            ;;
    esac
    ip=$(docker inspect $CIJW_DOCKER_PREFIX-$name | jq -r .[0].NetworkSettings.Networks.bridge.IPAddress)
    if [ -z "$ip" ]; then
        print_critical "Unable to get the $name service IP address" \
                        "This service seems to not be correctly started" \
                        "=> Try again with option \"$CI_JOB_NAME\".services.\"$name\".docker_run_args.privileged=true"
    fi
    print_note "STARTED: $name:$ip"
    CIJW_JOB_DOCKER_RUN_ARGS+=(--add-host "$name:$ip")
}

# Start services
for p in '."'"$CI_JOB_NAME"'"' ''; do
    for si in $([ "$(ci_jw_query -r "$p.services | type")" != "array" ] \
        || seq 0 $(($(ci_jw_query -r "$p.services | length") - 1))); do
        unset service_image service_alias service_commands
        service_commands=()
        s="$p.services[$si]"
        CIJW_SRV_DOCKER_RUN_ARGS=()
        case "$(ci_jw_query -r "$s | type")" in
            object)
                # Read the service name/image property
                if [ "$(ci_jw_query -r "$s.name | type")" == "string" ]; then
                    service_image="$(eval echo "$(ci_jw_query "$s.name")")"
                else
                    print_critical "$s: property 'name' not found"
                fi
                # Read entrypoint property
                if [ "$(ci_jw_query -r "$s.entrypoint | type")" == "array" ]; then
                    for i in $(seq 0 $(ci_jw_query -r "$s.entrypoint | length - 1")); do
                        service_commands+=("$(eval echo "$(ci_jw_query "$s.entrypoint[$i]")")")
                    done
                else
                    # Empty: NO entrypoint
                    service_commands+=("")
                fi
                # Read command property
                if [ "$(ci_jw_query -r "$s.command | type")" == "array" ]; then
                    for i in $(seq 0 $(ci_jw_query -r "$s.command | length - 1")); do
                        service_commands+=("$(eval echo "$(ci_jw_query "$s.command[$i]")")")
                    done
                fi
                # Read service alias property
                if [ "$(ci_jw_query -r "$s.alias | type")" == "string" ]; then
                    service_alias="$(eval echo "$(ci_jw_query "$s.alias")")"
                else
                    service_alias="$(echo "$service_image" | sed -E 's/:[^:\/]+//g' | sed -E 's/[^a-zA-Z0-9\._-]/__/g')"
                fi
                # Read the network service property
                case "$(ci_jw_query -r "$s.docker_run_args.network | type")" in
                    string)
                        CIJW_SRV_DOCKER_NET="$(eval echo "$(ci_jw_query "$s.docker_run_args.network")")"
                        ;;
                    ''|null)
                        # Copy the job network mode
                        CIJW_SRV_DOCKER_NET=$CIJW_JOB_DOCKER_NET
                        CIJW_SRV_DOCKER_RUN_ARGS+=(--network "$CIJW_SRV_DOCKER_NET")
                        ;;
                    *)
                        print_critical "$s.docker_run_args.network value must be a string"
                        ;;
                esac
                # Process docker run arguments
                process_docker_run_args CIJW_SRV_DOCKER_RUN_ARGS "$s"
                # Start service
                start_docker_service "$service_image" "$service_alias" "$CIJW_SRV_DOCKER_NET" "${service_commands[@]}"
                ;;
            string)
                # Copy the job network mode
                service_image="$(eval echo "$(ci_jw_query "$s")")"
                CIJW_SRV_DOCKER_RUN_ARGS+=(--network "$CIJW_SRV_DOCKER_NET")
                # Start service
                start_docker_service "$service_image" "$(echo "$service_image" | sed -E 's/:[^:\/]+//g' \
                    | sed -E 's/[^a-zA-Z0-9\._-]/__/g')" $CIJW_JOB_DOCKER_NET
                ;;
            *)
                ;;
        esac
    done
done

# Start...
if [ -n "${CIJW_JOB_DOCKER_COMMANDS[0]}" ]; then
    CIJW_JOB_DOCKER_IMAGE+=(--entrypoint "${CIJW_JOB_DOCKER_COMMANDS[0]}")
fi
print_info "Running the job $CI_JOB_NAME into the $CIJW_JOB_DOCKER_IMAGE docker container..."
docker run --rm --name "${HOSTNAME}-ci-job-wrapper" "${CIJW_DOCKER_RUN_ARGS[@]}" "${CIJW_JOB_DOCKER_RUN_ARGS[@]}" \
    --label "$CIJW_DOCKER_PREFIX" -w $CI_PROJECT_DIR $CIJW_JOB_DOCKER_IMAGE "${CIJW_JOB_DOCKER_COMMANDS[@]:1}" "$@"
